/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx      = 1;        /* border pixel of windows */
static const unsigned int snap          = 32;       /* snap pixel */
static const unsigned int gappih        = 5;        /* horiz inner gap between windows */
static const unsigned int gappiv        = 5;        /* vert inner gap between windows */
static const unsigned int gappoh        = 40;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov        = 40;       /* vert outer gap between windows and screen edge */
static       int smartgaps              = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar                = 1;        /* 0 means no bar */
static const int topbar                 = 1;        /* 0 means bottom bar */
static const char *fonts[]              = { "DejaVuSansM Nerd Font:size=10:style=Bold" };
static const char dmenufont[]           = "DejaVuSansM Nerd Font:size=10:style=Bold";
static const char col_black_alt[]       = "#270e05";
static const char col_light_blue_alt[]  = "#97aee4";
static const char col_white[]           = "#fef9cd";
static const char col_white_alt[]       = "#ffffff";
static const char *colors[][3]          = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_white, col_black_alt, col_black_alt },
	[SchemeSel]  = { col_white, col_light_blue_alt, col_white_alt },
};

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {"st", "-n", "spterm", "-g", "120x34", NULL };
const char *spcmd2[] = {"st", "-n", "spfm", "-g", "144x41", "-e", "ranger", NULL };
const char *spcmd3[] = {"keepassxc", NULL };
static Sp scratchpads[] = {
	/* name          cmd  */
	{"spterm",      spcmd1},
	{"spranger",    spcmd2},
	{"keepassxc",   spcmd3},
};

/* tagging */
static const char *tags[] = { "", "", "", "󰴱", "󰟵", "", "󱍚", "󰋅", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class         instance               title           tags mask               isfloating              monitor   float x,y,w,h         floatborderpx*/
        { "Emacs",       NULL,			NULL,		1 << 0,			0,		     	-1,        50,50,500,500,        1 },
        { NULL,          "st",			NULL,		1 << 1,			0,		     	-1,        50,50,500,500,        1 },
        { "qutebrowser", NULL,			NULL,		1 << 2,			0,		     	-1,        50,50,500,500,        1 },
	{ "Gcolor2",	 NULL,			NULL,		1 << 3,			1,	                -1,        50,50,500,500,        5 },
	{ "Gimp",	 NULL,			NULL,		1 << 3,			0,	                -1,        50,50,500,500,        1 },	
	{ "HandBrake",   NULL,			NULL,		1 << 6,			0,		     	-1,        50,50,500,500,        1 },
       	{ "kguitar",     NULL,			NULL,		1 << 7,			0,		     	-1,        50,50,500,500,        1 },
	{ "XConsole",    NULL,			NULL,		1 << 8,			0,		     	-1,        50,50,500,500,        1 },
	{ NULL,		 "spterm",		NULL,		SPTAG(0),		1,		     	-1,        50,50,500,500,        5 },
	{ NULL,		 "spfm",		NULL,		SPTAG(1),		1,		     	-1,        50,50,500,500,        5 },
	{ NULL,		 "keepassxc",	        NULL,		SPTAG(2),		0,			-1,        50,50,500,500,        5 },
};


/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int nstack      = 0;    /* number of clients in primary stack area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function, { nmaster, nstack, layout, master axis, stack axis, secondary stack axis } */
	{ "[]=",      flextile,         { -1, -1, -SPLIT_VERTICAL, TOP_TO_BOTTOM, TOP_TO_BOTTOM, 0, NULL } }, // default tile layout
	{ "><>",      NULL,             {0} },    /* no layout function means floating behavior */
	{ "[M]",      flextile,         { -1, -1, NO_SPLIT, MONOCLE, MONOCLE, 0, NULL } }, // monocle
	{ "|||",      flextile,         { -1, -1, SPLIT_VERTICAL, LEFT_TO_RIGHT, TOP_TO_BOTTOM, 0, NULL } }, // columns (col) layout
	{ ">M>",      flextile,         { -1, -1, FLOATING_MASTER, LEFT_TO_RIGHT, LEFT_TO_RIGHT, 0, NULL } }, // floating master
	{ "[D]",      flextile,         { -1, -1, SPLIT_VERTICAL, TOP_TO_BOTTOM, MONOCLE, 0, NULL } }, // deck
	{ "TTT",      flextile,         { -1, -1, SPLIT_HORIZONTAL, LEFT_TO_RIGHT, LEFT_TO_RIGHT, 0, NULL } }, // bstack
	{ "===",      flextile,         { -1, -1, SPLIT_HORIZONTAL, LEFT_TO_RIGHT, TOP_TO_BOTTOM, 0, NULL } }, // bstackhoriz
	{ "|M|",      flextile,         { -1, -1, SPLIT_HORIZONTAL, LEFT_TO_RIGHT, TOP_TO_BOTTOM, 0, monoclesymbols } }, // centeredmaster
	{ ":::",      flextile,         { -1, -1, NO_SPLIT, GAPPLESSGRID, GAPPLESSGRID, 0, NULL } }, // gappless grid
	{ "[\\]",     flextile,         { -1, -1, NO_SPLIT, DWINDLE, DWINDLE, 0, NULL } }, // fibonacci dwindle
	{ "(@)",      flextile,         { -1, -1, NO_SPLIT, SPIRAL, SPIRAL, 0, NULL } }, // fibonacci spiral
	{ "[T]",      flextile,         { -1, -1, SPLIT_VERTICAL, LEFT_TO_RIGHT, TATAMI, 0, NULL } }, // tatami mats
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-vi", NULL, "-m", dmenumon, "-fn", dmenufont };
static const char *termcmd[]  = { "st", NULL };

static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } }, 
	{ MODKEY,                       XK_q,      spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_e,      spawn,          SHCMD("emacs") },
	{ MODKEY|ShiftMask,             XK_q,      spawn,          SHCMD("qutebrowser") },
	{ MODKEY,                       XK_g,      spawn,          SHCMD("gimp") },
	{ MODKEY,                       XK_c,      spawn,          SHCMD("gcolor2") },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
        { MODKEY|ShiftMask,             XK_b,      toggleborder,   {0} },
	{ MODKEY,                       XK_h,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_t,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_i,      incnmaster,     {.i = -1 } },
	{ MODKEY|ControlMask,           XK_i,      setnmaster,     {.i = 1 } },
//	{ MODKEY|ControlMask,           ,          incnstack,      {.i = +1 } },
//	{ MODKEY|ControlMask,           ,          incnstack,      {.i = -1 } },
	{ MODKEY,                       XK_n,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_d,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_d,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_n,      setcfact,       {.f = -0.25} },
	{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY|Mod4Mask,              XK_u,      incrgaps,       {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_i,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_o,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_6,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_0,      togglegaps,     {0} },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_k,      killclient,     {0} },
	{ MODKEY,                       XK_s,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
        { MODKEY|ShiftMask,             XK_m,      togglefullscr,  {0} },
	{ MODKEY|ControlMask,           XK_a,      rotatelayoutaxis, {.i = +1 } },  /* flextile, 1 = layout axis */
	{ MODKEY|ControlMask,           XK_o,      rotatelayoutaxis, {.i = +2 } },  /* flextile, 2 = master axis */
	{ MODKEY|ControlMask,           XK_e,      rotatelayoutaxis, {.i = +3 } },  /* flextile, 3 = stack axis */
	{ MODKEY|ControlMask,           XK_u,      rotatelayoutaxis, {.i = +4 } },  /* flextile, 4 = secondary stack axis */
	{ MODKEY|ControlMask|ShiftMask, XK_a,      rotatelayoutaxis, {.i = -1 } },  /* flextile, 1 = layout axis */
	{ MODKEY|ControlMask|ShiftMask, XK_o,      rotatelayoutaxis, {.i = -2 } },  /* flextile, 2 = master axis */
	{ MODKEY|ControlMask|ShiftMask, XK_e,      rotatelayoutaxis, {.i = -3 } },  /* flextile, 3 = stack axis */
	{ MODKEY|ControlMask|ShiftMask, XK_u,      rotatelayoutaxis, {.i = -4 } },  /* flextile, 4 = secondary stack axis */
	{ MODKEY|ControlMask,           XK_Return, mirrorlayout,   {1} },           /* flextile, flip master and stack areas */
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,            		XK_y,  	   togglescratch,  {.ui = 0 } },
	{ MODKEY,            		XK_z,	   togglescratch,  {.ui = 1 } },
	{ MODKEY,            		XK_x,	   togglescratch,  {.ui = 2 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_x,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

